const fs = require('fs');
const path = require('path');
const { execSync } = require('child_process');

const inquirer = require('inquirer');
const kebabCase = require('lodash.kebabcase');
const camelCase = require('lodash.camelcase');
const pick = require('lodash.pick');

module.exports = function (argv) {
  const pathToWrite = path.join(argv.cwd, argv.unreleasedDir);
  if (!fs.existsSync(pathToWrite)) {
    fs.mkdirSync(pathToWrite, { recursive: true });
  }

  let defaultTitle = argv._[1];
  if (argv.amend) {
    const commitMessage = execSync(
      `cd ${argv.cwd} && git log --format=%B -n 1`
    ).toString();
    defaultTitle = argv._[1] || commitMessage.trim().replace(/(\n)+/g, ' ');
  }
  let author = argv.author;
  if (argv.gitUsername) {
    author = execSync(`cd ${argv.cwd} && git config user.name`)
      .toString()
      .trim();
  }

  const properties = [
    {
      name: 'title',
      message: 'Entry title',
      type: 'input',
      default: defaultTitle,
    },
    {
      name: 'merge_request',
      message: 'Merge Request ID',
      type: 'input',
      default: argv.mergeRequest,
    },
    {
      name: 'author',
      message: 'External Contributor',
      type: 'input',
      default: author,
    },
    {
      name: 'type',
      message: 'Type',
      type: 'list',
      default: argv.type,
      choices: argv.config.types,
    },
  ];

  const extraProperties = (argv.config.extraProperties || []).map((e) => {
    return Object.assign({}, e, { default: argv[camelCase(e.name)] });
  });

  inquirer.prompt(properties.concat(extraProperties)).then((answers) => {
    const picked = pick(
      answers,
      extraProperties.map((p) => p.name)
    );
    const extraPropertiesAnswers = Object.keys(picked).map((key) => {
      return `${key}: ${answers[key]}`;
    });
    const entry = `---
title: "${answers.title.replace(/"/g, '\\"').trim()}"
type: ${answers.type}
${
  answers.merge_request != ''
    ? 'merge_request: ' + answers.merge_request.trim()
    : ''
} 
${extraPropertiesAnswers.join('\n')}
`;
    const shortTitle = kebabCase(answers.title).substr(0, 50);
    const filePath = path.join(
      pathToWrite,
      answers.merge_request
        ? `${answers.merge_request}-${shortTitle}.yml`
        : `${shortTitle}.yml`
    );
    if (argv.dryRun) {
      console.log(entry);
      return;
    }
    if (fs.existsSync(filePath) && !argv.force) {
      console.error(
        `error ${filePath} already exists! Use '--force' to overwrite.`
      );
      process.exit(1);
    }
    fs.writeFile(filePath, entry, (err) => {
      if (err) {
        console.log('could not save entry to file');
        console.error(err);
        process.exit(1);
      }
      if (argv.amend) {
        execSync(
          `cd ${argv.cwd} && git add ${filePath} && git commit --amend --no-edit`
        );
      }
      console.log('file was written to', filePath);
    });
  });
};
