const path = require('path');

function checkAndInsertFullstop(replaced) {
  const [pre, ...rest] = replaced.split('__.__');
  if (rest.length == 0) {
    return pre;
  }
  const lastCharacter = pre.slice(-1);
  const fullStop = lastCharacter == '.' || lastCharacter == '!' ? '' : '.';
  return [pre, fullStop].concat(rest).join('');
}

function replaceTemplate(
  template,
  object,
  blocks = {},
  fail,
  noWarnings,
  tryBlock = true
) {
  return template.replace(/\$\{(\w*)\}/g, function () {
    const args = Array.prototype.slice.call(arguments);
    const [pattern, property, index, fullString] = args;
    if (object[property] == null) {
      if (!noWarnings) {
        console.error(
          'could not find property',
          property,
          'in',
          path.basename(object.__filePath)
        );
      }
      if (fail) {
        process.exit(1);
      }
    }
    if (object[property] != null) {
      const block = blocks[property];
      if (tryBlock && block != null) {
        return replaceTemplate(block, object, {}, fail, noWarnings, false);
      }
      return object[property];
    }
    return '';
  });
}

function stripMergeRequest(rendered, pattern, stripLink = true) {
  if (stripLink) {
    return rendered.replace(new RegExp(pattern, 'g'), '');
  }
  return rendered;
}

function renderTemplate(argv, object) {
  const template = argv.config.entryTemplate;
  const tmp1 = replaceTemplate(
    template,
    object,
    argv.config.templateBlocks,
    argv.fail,
    argv.noWarnings
  );
  const tmp2 = checkAndInsertFullstop(tmp1);
  return stripMergeRequest(tmp2, argv.config.stripLinkPattern, argv.stripLink);
}

module.exports = {
  renderTemplate,
  stripMergeRequest,
};
