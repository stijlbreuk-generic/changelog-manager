const fs = require('fs');
const path = require('path');

const inquirer = require('inquirer');
const yaml = require('js-yaml');
const semver = require('semver');

const utilty = require('./utility');

function updateChangelogFile(
  fileContent,
  newVersion,
  newContent,
  releaseHeading
) {
  const pattern = new RegExp(`${releaseHeading}\\s([^\\s]*)`);
  let releases = [];
  let tmp = [];
  let introLines = [];
  fileContent.split('\n').forEach((line) => {
    if (line.indexOf(`${releaseHeading} `) == 0) {
      const version = (line.match(pattern) || [])[1];
      releases.unshift({
        version: version,
        content: [line],
      });
    } else {
      if (releases.length == 0) {
        introLines.push(line);
        return;
      }
      releases[0].content.push(line);
    }
  });
  releases = releases.map((r) => {
    return {
      version: r.version,
      content: r.content.join('\n'),
    };
  });
  releases.push({
    version: newVersion,
    content: newContent,
  });
  const sorted = releases.sort((a, b) => {
    return semver.rcompare(a.version, b.version);
  });
  const introSection = introLines.join('\n');
  const releasesSection = sorted.map((i) => i.content).join('\n');
  return `${introSection}\n${releasesSection}\n`; // add a newline to the end
}

module.exports = function (argv) {
  const pathToScan = path.join(argv.cwd, argv.unreleasedDir);
  const pathToWrite = path.join(argv.cwd, argv.releasedDir);

  function useAllEntries(version) {
    listAllFiles((err, files) => {
      if (err) {
        console.error(err);
        return process.exit(1);
      }
      aggregate(version, files);
    });
  }

  function capitalizeFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  var allEntriesPrompt = {
    type: 'list',
    name: 'allEntries',
    message: 'Use all entries (YML files) in changelogs/unreleased?',
    choices: ['Yes', 'No'],
  };

  function init() {
    if (argv.ci) {
      const version = argv['release-version'] || '0.0.0-SNAPSHOT';
      useAllEntries(version);
    } else {
      releaseVersionPrompt().then((result) => {
        inquirer.prompt(allEntriesPrompt).then((answers) => {
          if (answers.allEntries === 'Yes') {
            useAllEntries(result.version);
          } else {
            selectEntries(result.version);
          }
        });
      });
    }
  }

  function releaseVersionPrompt() {
    return inquirer.prompt({
      type: 'input',
      name: 'version',
      message: 'Release version',
      default: argv.releaseVersion,
      validate: function (input) {
        if (argv.noSemver) {
          return true;
        }
        const filtered = input.replace(/\(.*\)/, '').trim();
        if (semver.valid(filtered)) {
          return true;
        }
        return 'Invalid semantic version';
      },
    });
  }

  function listAllFiles(callback) {
    fs.readdir(pathToScan, (err, files) => {
      if (err) {
        console.log('could not scan directory', pathToScan);
        return callback(err);
      }
      const filteredFiles = files.filter((file) => file.substr(0, 1) != '.');
      callback(null, filteredFiles);
    });
  }

  function selectEntries(version) {
    listAllFiles((err, files) => {
      if (err) {
        console.error(err);
        return process.exit(1);
      }
      inquirer
        .prompt({
          type: 'checkbox',
          name: 'selectedEntries',
          message: 'Select entries which should be released',
          choices: files,
          pageSize: 20,
        })
        .then((answers) => {
          aggregate(version, answers.selectedEntries);
        });
    });
  }

  function aggregate(newVersion, fileNames) {
    if (!argv.dryRun) {
      console.log(`converting ${fileNames.length} files`);
    }
    const types = {};
    const files = fileNames.map((fileName) => path.join(pathToScan, fileName));
    files.forEach((filePath) => {
      const content = fs.readFileSync(filePath, 'utf8');
      const object = yaml.load(content);
      if (types[object.type] == null) {
        types[object.type] = [];
      }
      types[object.type].push(Object.assign(object, { __filePath: filePath }));
    });
    const markdownTemplates = [];
    for (type in types) {
      const entryLength = types[type].length;
      const typeHeader = [
        `${argv.config.typeHeading} ${capitalizeFirst(
          type
        )} (${entryLength} change${entryLength > 1 ? 's' : ''})`,
      ];
      const entries = types[type].map((entry) => {
        const rendered = utilty.renderTemplate(argv, entry);
        return `- ${rendered}`;
      });
      markdownTemplates.push(typeHeader + '\n\n' + entries.join('\n'));
    }
    const newContent =
      `${argv.config.releaseHeading} ` +
      newVersion +
      '\n\n\n' +
      markdownTemplates.join('\n\n\n') +
      '\n';
    const changelogFileName = argv.out || argv.config.changelogFile;
    const releaseFilePath = path.join(pathToWrite, changelogFileName);
    let currentContent = '';
    try {
      currentContent = fs.readFileSync(releaseFilePath, 'utf8');
    } catch (err) {}
    const updated = updateChangelogFile(
      currentContent,
      newVersion,
      newContent,
      argv.config.releaseHeading
    );

    if (argv.dryRun) {
      console.log(newContent);
      return;
    }
    fs.writeFileSync(releaseFilePath, updated);
    console.log('changelog was updated', releaseFilePath);
    files.forEach((file) => {
      fs.unlinkSync(file);
    });
    console.log(`removed ${files.length} entries from ${pathToScan}`);
  }

  init();
};
