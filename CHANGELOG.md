# ChangeLoh Managet

Changelog manager for Stijlbreuk

## 1.0.0-rc.4


### Added (2 changes)

- Provide `--release-version` arg for release subcommand.
- Add new flag to run release subcommand in CI (disabled interactive mode).


### Changed (1 change)

- [entry] Do not write out merge_request if it was not filled.

## 1.0.0-rc.3


### Fixed (1 change)

- Add new line to the end of the CHANGELOG.md.


### Added (1 change)

- Allow to overwrite default file name for CHANGELOG.md.


### Changed (1 change)

- Do not filter for yml file extension, use all files except starting with a dot.

## 1.0.0-rc.2


### Fixed (1 change)

- Fix rendering block templates.


### Changed (1 change)

- Move fail option to release subcommand and add no-warnings option.

## 1.0.0-rc.1


### Added (3 changes)

- Allow to configure additional properties in YAML files.
- Allow to define template blocks which are omitted when a certain property is not available.
- Provide --amend to use last git commit and amend generated file to previous commit.


### Changed (7 changes)

- Allow to use independent directories for unreleased and released files.
- Change option from dry to dry-run.
- Change subcommand entry to add.
- Fail when changelog entry already exists, except using -f.
- Provide author property and allow to fill it from git config.
- Validate version (semantic version) when creating a new entry.
- Use same file to add releases. Also sort by semantic versioning.


### Fixed (2 changes)

- Fix CLI options (some default values and typo in a variable).
- Fix dry mode for add subcommand.
