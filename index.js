#!/usr/bin/env node

const fs = require('fs');
const path = require('path');
const os = require('os');
const process = require('process');

const yaml = require('js-yaml');

const createEntry = require('./create-entry');
const createRelase = require('./create-release');
const utility = require('./utility');

// default config
let config = {
  releasedDir: '.',
  unreleasedDir: 'changelogs/unreleased',
  types: [
    'added',
    'fixed',
    'changed',
    'deprecated',
    'removed',
    'security',
    'performance',
    'other',
  ],
  releaseHeading: '##',
  typeHeading: '###',
  changelogFile: 'CHANGELOG.md',
  templateBlocks: {
    merge_request: ' !${merge_request}',
    author: ' (${author})',
  },
  entryTemplate: '${title}__.__${merge_request}${author}',
  stripLinkPattern: '(!w*)',
};

const homeDirectory = os.homedir();
let rawConfig = null;

try {
  //Local directory
  const customConfigPath = path.join(process.cwd(), '.changelog-manager.yml');
  rawConfig = fs.readFileSync(customConfigPath, 'utf8');
} catch (err) {
  try {
    //Home directory
    const customConfigPath = path.join(homeDirectory, '.changelog-manager.yml');
    rawConfig = fs.readFileSync(customConfigPath, 'utf8');
  } catch (err) {
    // ignore, use default
  }
}

if (rawConfig != null) {
  const parsedConfig = yaml.load(rawConfig);
  config = Object.assign(config, parsedConfig);
}

const yargs = require('yargs');
const globalArgv = yargs
  .usage('$0 <subcommand>')
  .command(
    'add [args]',
    'create a changelog entry in YAML format (interactive)'
  )
  .command(
    'release [args]',
    'aggregate entry files and convert them to a release file in markdown format (interactive)'
  )
  .command(
    'strip-link <file>',
    'strip links to merge request in an existing markdown file and print it to stdout'
  )

  .string('cwd')
  .describe('cwd', 'overwrite current working directory ($PWD)')
  .string('released-dir')
  .describe(
    'released-dir',
    'directory where to generate releases files (Markdown)'
  )
  .string('unreleased-dir')
  .default('released-dir', 'changelogs')
  .describe('unreleased-dir', 'directory where to create unreleased YAML files')
  .default('unreleased-dir', 'changelogs/unreleased')
  .global('fail', false)
  .global('cwd', false)
  .global('released-dir', false)
  .global('unreleased-dir', false)
  .global('version', false)
  .help('help', 'show global help, use -h for subcommands')
  .demandCommand(1, 'must provide a valid subcommand').argv;

// default values (using yargs API) are not working (set) for non-global options
globalArgv.fail = false;
globalArgv.cwd = globalArgv.cwd || process.cwd();
globalArgv.releasedDir = globalArgv.releasedDir || config.releasedDir;
globalArgv.unreleasedDir = globalArgv.unreleasedDir || config.unreleasedDir;
globalArgv.config = config;

const command = globalArgv._[0];

if (command === 'add') {
  const argv = yargs
    .reset()
    .usage('$0 add [title] [args]')
    .option('d', {
      type: 'boolean',
      alias: 'dry-run',
      describe: 'option to prevent actually writing or committing anything',
    })
    .option('m', {
      type: 'string',
      alias: 'merge-request',
      describe: 'option to automatically fill in the merge_request',
    })
    .option('t', {
      type: 'string',
      alias: 'type',
      choices: globalArgv.config.types,
      describe: 'set the type',
    })
    .option('a', {
      type: 'boolean',
      alias: 'amend',
      describe:
        'use last git commit as default for title and ammend generated file to previous commit',
    })
    .option('u', {
      type: 'boolean',
      alias: 'git-username',
      describe:
        'option to automatically fill in the author value with your configured Git user.name',
    })
    .option('f', {
      type: 'boolean',
      alias: 'force',
      describe: 'overwrite an existing changelog entry if it already exists',
    })
    .help('h')
    .example(
      '$0 add',
      'interactive CLI to create a changelog entry as a YAML file'
    ).argv;

  createEntry(Object.assign({}, globalArgv, argv));
} else if (command === 'release') {
  const argv = yargs
    .reset()
    .usage('$0 release [options]')
    .option('d', {
      type: 'boolean',
      alias: 'dry-run',
      describe:
        'option to prevent actually writing or deleting anything. Prints content to stdout',
    })
    .option('o', {
      alias: 'out',
      type: 'string',
      describe:
        'file name where to write the output (realtive to released-dir)',
      default: config.changelogFile,
    })
    .option('s', {
      type: 'boolean',
      alias: 'strip-link',
      describe: 'omit links to merge request',
    })
    .option('n', {
      type: 'boolean',
      alias: 'noWarnings',
      describe:
        'do not print warnings if variables in a template cannot be replaced',
    })
    .option('fail', {
      type: 'boolean',
      describe:
        'stop and exist with non zero status code if variables in template cannot be replaced',
    })
    .option('ci', {
      type: 'boolean',
      describe:
        'disable interactive mode (by default all YAML files will be used)',
    })
    .option('releaseVersion', {
      type: 'string',
      describe: 'prefill release version',
    })
    .option('noSemver', {
      type: 'boolean',
      describe: 'do not validate for semantic versioning',
    })

    .example(
      '$0 release',
      'interactive CLI to aggegate YAML files and convert them into a markdown file, ' +
        'selected YAML files will be deleted automtacially'
    )
    .help('h').argv;

  createRelase(Object.assign({}, globalArgv, argv));
} else if (command === 'strip-link') {
  const argv = yargs
    .reset()
    .usage('$0 strip-link <release.md>')
    .example('$0 strip-link changeslogs/unreleased/release-1.0.0.md')
    .help('h').argv;

  const relativeFilePath = argv._[1];
  let filePathToStrip = relativeFilePath;
  // allow to specify absolute paths
  if (relativeFilePath.substr(0, 1) != '/') {
    filePathToStrip = path.join(globalArgv.cwd, relativeFilePath);
  }
  const releaseFileContent = fs.readFileSync(filePathToStrip, 'utf8');
  const result = utility.stripMergeRequest(
    releaseFileContent,
    config.stripLinkPattern
  );
  console.log(result);
} else {
  yargs.showHelp();
  process.exit(1);
}
